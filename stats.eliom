open Eliom_lib
open Eliom_content
open Html5.D
       
module Stats_app =
  Eliom_registration.App (
      struct
	let application_name = "stats"
      end)
			 
let main_service =
  Eliom_service.App.service ~path:[] ~get_params:Eliom_parameter.unit ()
			    
let () =
  Stats_app.register
    ~service:main_service
    (fun () () ->
     Lwt.return
        (Eliom_tools.F.html
           ~title:"stats"
           ~css:[["css";"stats.css"]]
           Html5.F.(body [
			p [a ~service:Nba.nba_service
			     [pcdata "Go to nba stats"] ()]
		      ]
		   )))
    
