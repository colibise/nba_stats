open Eliom_lib
open Eliom_content
open Html5.D

(* Aux *)
open Printf  

(* Json *)
open Yojson  
open Yojson.Basic
open Yojson.Basic.Util

(* LWT *)
open Lwt
let ( >>= ) = bind

let append_lwt x lwt_xs =
  lwt_xs >>= fun xs -> return (x::xs)

let rec concat_lwt lwt_xs lwt_ys =
  lwt_xs >>= function
	   | []      -> lwt_ys
	   | (x::xs) -> append_lwt x (concat_lwt (return xs) lwt_ys)  

(* Types *)

open Type
				
(* Services *)
				
let nba_service =
  Eliom_service.Http.service
    ~path:["nba"]
    ~get_params:(Eliom_parameter.unit)
    ()

let team_service = 
  Eliom_service.Http.service
    ~path:["team"]
    ~get_params:(Eliom_parameter.(suffix (string "team_id")))
    ()

(* Url treatments *)

exception Empty_Frame_Content
    
let make_url ls =
  "http://stats.nba.com/stats/" ^ List.fold_right (^) ls ""

let team_info ?(id_League="00") ?(season="2014-15") id_Team =
  make_url
    ["teaminfocommon?"
    ; sprintf "LeagueID=%s" id_League
    ; sprintf "&SeasonType=Regular+Season"
    ; sprintf "&TeamID=%d" id_Team
    ; sprintf "&season=%s" season
    ]
    	    
let open_url_frame ?(size=4096) url =
  let open Ocsigen_stream in 
  Ocsigen_http_client.get_url url >>= fun c ->
  c.frame_content
  |> (function
       | Some s -> s
       | None   -> raise Empty_Frame_Content)
  |> get
  |> string_of_stream size

(* TEAM CATCHING *)
		      
let get_team_by_id n =
  let open List in
  let url = team_info n in
  open_url_frame url >>= fun s ->
  let results =
    from_string s
    |> member "resultSets"
    |> to_list in
  let ks = results |> hd |> member "headers" |> to_list
  and vs = results |> hd |> member "rowSet"  |> to_list |> hd |> to_list
  in
  let nks = combine ks vs in
  return @@
    { name = assoc (`String "TEAM_NAME") nks |> to_string
    ; city = assoc (`String "TEAM_CITY") nks |> to_string
    ; id   = assoc (`String "TEAM_ID"  ) nks |> to_int
    }

let range m n =
  let rec aux acc m n = 
    if m > n then acc
    else get_team_by_id n >>= fun t ->
	 aux (append_lwt t acc) m (pred n)
  in aux (return []) m n

(* this is supposed catch all teams *)
let initial_id = 1610612737
let final_id   = 1610612766
let all_teams  = range initial_id final_id

(* Display functions *)

(* create the form for choosing a team *)

exception Empty_options

let create_select_form ts name =
  let option_team t =
    Option ( []
	   , string_of_int t.id
	   , Some (pcdata (t.city ^ " " ^ t.name))
	   , false )
  in
  match ts with
     | [] -> raise Empty_options
     | t :: ts ->
	[p [pcdata "Choose a team ";
	    string_select
	      ~name:name
	      (option_team t)
	      (List.map option_team ts)
	    ;
	      string_input ~input_type:`Submit ~value:"Choose" ()
	   ]
	]

(* display all teams with links *)
let display_all_teams ts =
  ul (List.map (fun t -> 
		li [a
		      ~service:team_service
		      [pcdata (t.city ^ " " ^ t.name)]
		      (string_of_int t.id)
		   ]) ts)
     
let nba_display () () =
  all_teams >>= fun ts ->
  let choose_team = get_form team_service (create_select_form ts) in
  return @@
    Eliom_tools.D.html
      ~title:"NBA Stats"
      ~css:[["css"; "style.css"]]
      (body [ h1 [i [pcdata "NBA STATS"]]
	    ; choose_team
	    ; display_all_teams ts
	    ]
      )

(* display stats *)

let display_stats s =
  let open List in
  let open Yojson.Basic in
  let open Yojson.Basic.Util in
  let results =
    from_string s
    |> member "resultSets"
    |> to_list in
  let ks = results |> hd |> member "headers" |> to_list
  and vs = results |> hd |> member "rowSet"  |> to_list |> hd |> to_list
  in
  let nks = combine ks vs in
  let team =
    { name = assoc (`String "TEAM_NAME") nks |> to_string
    ; city = assoc (`String "TEAM_CITY") nks |> to_string
    ; id   = assoc (`String "TEAM_ID"  ) nks |> to_int
    }
  in  
  let string_filter2 x y acc = match (x,y) with 
    | `String s1 , `String s2 ->
       tr [td [pcdata s1]; td [pcdata s2]] :: acc
    | _ -> acc
  in
  [ h1 [i [pcdata (team.city ^ " " ^ team.name)]]
  ; table @@ fold_right2 string_filter2 ks vs []]

let team_service_display team_id () =
  let info = open_url_frame (team_info (int_of_string (team_id)))
  in info >>= fun s ->
     return @@
       Eliom_tools.D.html
	 ~title:"NBA Stats"
	 ~css:[["css"; "style.css"]]
	 (body (display_stats s))


(* Services Launching *)

let _ =
  Eliom_registration.Html5.register
    ~service:nba_service
    nba_display ;
  
  Eliom_registration.Html5.register
    ~service:team_service
    team_service_display
